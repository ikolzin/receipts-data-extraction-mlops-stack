# Receipts Data Extraction - MLOps Stack

This is a companion project to [Recepits Data Extraction](https://gitlab.com/ikolzin/receipts-data-extraction).
Here we set up a MLOps stack for our service:
- PostgreSQL (+ pgadmin) as a database for MLFlow
- Minio S3 storage
- MLFlow (+ nginx reverse proxy) for experiment monitoring and model deployment
- Prometheus monitoring
- Grafana visualisation

------------

**Installation:**

- rename .env.template to .env and fill in your credentials. Note that if you plan to use Minio for S3 storage you will need to create an access key in the UI first. If you plan to use a different S3 provider you will need to remove Minio section from docker-compose.yaml
- if you want to use nginx for authentication to MLFlow, you will need a [.htpasswd](https://gitlab.com/ikolzin/receipts-data-extraction/-/blob/main/docker/mlflow-stack/nginx-image/auth/nginx.htpasswd.template?ref_type=heads) file. Otherwise, delete the nginx_mlflow section from docker-compose.
- run  `docker-compose up -d --build`
- to connect to MLFlow from your project, rename [.env.template](https://gitlab.com/ikolzin/receipts-data-extraction/-/blob/main/.env.template?ref_type=heads) in the project root to .env and fill in your credentials. 
- to use Prometheus to monitor your FastAPI instance, change the value of `targets` in monitoring/prometheus.yml